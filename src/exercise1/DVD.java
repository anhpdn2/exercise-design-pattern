package exercise1;

public class DVD extends Item {
    private String star;

    public DVD(String title, int price, String star) {
        super(title, price);
        this.star = star;
    }

    public String getStar() {
        return star;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
