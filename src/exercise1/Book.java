package exercise1;

public class Book extends Item {
    private String author;
    private String publisher;

    public Book(String title, int price, String author, String publisher) {
        super(title, price);
        this.author = author;
        this.publisher = publisher;
    }

    public String getAuthor() {
        return author;
    }

    public String getPublisher() {
        return publisher;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
