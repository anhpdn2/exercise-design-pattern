package exercise1;

public class ShortInfo implements Visitor {

    @Override
    public void visit(Book book) {
        System.out.println("Book: " + book.getTitle() + " - " + book.getAuthor());
    }

    @Override
    public void visit(DVD dvd) {
        System.out.println("DVD: " + dvd.getTitle() + " - " + dvd.getStar());

    }
}
