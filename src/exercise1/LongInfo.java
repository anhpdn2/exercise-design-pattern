package exercise1;

public class LongInfo implements Visitor {

    @Override
    public void visit(Book book) {
        System.out.println("Book: " + book.getTitle() + " - " + book.getAuthor());
        System.out.println("Book: " + book.getPrice() + " - " + book.getPublisher());
    }

    @Override
    public void visit(DVD dvd) {
        System.out.println("DVD: " + dvd.getTitle() + " - " + dvd.getStar());
        System.out.println("DVD: " + dvd.getPrice());

    }
}
