package exercise1;

public class Main {
    public static void main(String[] args) {
        Book b1 = new Book("Sand & Foam", 10, "Khalil Gibran", "ducanh");
        Book b2 = new Book("Sand & Foam ep2", 10, "Khalil Ducanh", "ducanh");

        Book b3 = new Book("Sand & Foam ep2", 10, "Khalil Minh", "minh");
        DVD d1 = new DVD("fast and furious", 10, "5");
        DVD d2 = new DVD("fast and furious 9", 10, "2");
        b1.accept(new ShortInfo());
        b2.accept(new LongInfo());
        d1.accept(new ShortInfo());
        d2.accept(new LongInfo());
    }
}
