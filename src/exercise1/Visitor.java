package exercise1;

public interface Visitor {
    void visit(Book book);
    void visit(DVD dvd);
}
