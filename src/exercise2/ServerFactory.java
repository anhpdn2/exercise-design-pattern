package exercise2;

public class ServerFactory implements AbstractFactory{
    @Override
    public Computer createComputer(String ram, String hdd, String cpu) {
        return new Server(ram, hdd, cpu);
    }
}
