package exercise2;

public class PCFactory implements AbstractFactory{
    @Override
    public Computer createComputer(String ram, String hdd, String cpu) {
        return new PC(ram, hdd, cpu);
    }
}
