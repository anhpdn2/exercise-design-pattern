package exercise2;

public interface AbstractFactory{
    Computer createComputer(String ram, String hdd, String cpu);
}
