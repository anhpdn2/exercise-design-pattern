package exercise2;

public class Main {
    public static void main(String[] args) {
        /*Computer pc = ComputerFactory.getComputer("pc", "2GB", "500GB", "i712th");
        Computer server = ComputerFactory.getComputer("server", "2GB", "500GB", "i712th");
        System.out.println(pc);
        System.out.println(server );*/
        AbstractFactory factory = new PCFactory();
        Computer pc = factory.createComputer("6gb", "2GB",  "i712th");
        System.out.println(pc);
    }
}
