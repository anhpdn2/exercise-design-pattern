package exercise4;

import java.io.IOException;
import java.io.Serializable;

public class Point implements Serializable {
    private int x;
    private int y;

    private CareTaker taker = new CareTaker();

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void translate(int dx, int dy) throws IOException {
        taker.addMemento(createMemento());
        this.x += dx;
        this.y += dy;
    }

    public void set(int x, int y) throws IOException {
        taker.addMemento(createMemento());
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Current state: x =" + x + " , y =" + y;
    }

    public Memento createMemento() {
        return new Memento(x,y);
    }

    public void restore(int index) throws IOException {
        taker.addMemento(createMemento());
        Memento memento = (Memento)taker.restore(index);
        this.x = memento.x;
        this.y = memento.y;
    }
}
