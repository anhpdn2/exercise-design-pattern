package exercise4;

import java.io.*;
import java.util.ArrayList;

public class CareTaker implements Serializable {
    public void addMemento(InterfaceMemento memento) throws IOException {
        File f = new File("Memento.obj");
        ObjectOutputStream st = null;
        if (!f.exists()) {
            st = new ObjectOutputStream(new FileOutputStream(f));

        }else {
            st =  new ObjectOutputStream(new FileOutputStream(f, true)) {
              protected void writeStreamHeader() throws IOException {
                  reset();
              }
            };
        }
        st.writeObject(memento);
        st.close();
    }

    public InterfaceMemento restore(int index) {
        ArrayList<InterfaceMemento> list = new ArrayList<>();
        ObjectInputStream st = null;
        File f = new File("Memento.obj");
        Object ob;
        try {
            st = new ObjectInputStream(new FileInputStream(f));
            while (true) {
                ob = st.readObject();
                list.add((InterfaceMemento) ob);
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            try {
                st.close();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            InterfaceMemento mem = list.remove(index);
            ObjectOutputStream stOut = new ObjectOutputStream(new FilterOutputStream(f));
            for (InterfaceMemento obj : list) {
                stOut.writeObject(obj);
            }
            stOut.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
