package exercise4;

public abstract class InterfaceMemento {
    private int x;
    private int y;

    public InterfaceMemento(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void restore(int index) {
        Memento memento = (Memento)
        this.x = memento.x;
        this.y = memento.y;
    }
}
