package exercise4;

import java.io.Serializable;

public class Memento extends InterfaceMemento implements Serializable {
    public int x;
    public int y;

    public Memento(int x, int y, int x1, int y1) {
        super(x, y);
        this.x = x1;
        this.y = y1;
    }
}
