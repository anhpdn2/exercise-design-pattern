package exercise3;

public class AC extends Option{
    public AC(CarInterface car, int priceOption) {
        super(car, priceOption);
    }

    public void display() {
        car.display();
        System.out.println("Option: AirCondition" + getPriceOption());
    }
}
