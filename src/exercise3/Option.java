package exercise3;

public abstract class Option {
    CarInterface car;
    int priceOption;

    public Option(CarInterface car, int priceOption) {
        this.car = car;
        this.priceOption = priceOption;
    }

    public int getPriceOption() {
        return car.getPrice() + priceOption;
    }
}
