package exercise3;

public class NissanSunny extends Car {
    public NissanSunny(int price, int year) {
        super(price, year);
    }

    public void display() {
        System.out.println("Nissan Sunny: " + getYear() + " price " + getPrice());
    }
}
