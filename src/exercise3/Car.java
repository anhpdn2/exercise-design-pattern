package exercise3;

public abstract class Car {
    int price;
    int year;

    public Car(int price, int year) {
        this.price = price;
        this.year = year;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
